import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import LoginComponent from './Pages/login/'
import AdminComponent from './Pages/admin/'
import tombsMapComponent from './Pages/map/'
import homeComponent from './Pages/home/'
import listaComponent from './Pages/lista/'
import sepulturaComponente from './Pages/sepultura/'

import registerComponent from './Forms/FormRegister'
import cemiterioComponent from './Forms/FormCemiterio'
// import formTombsComponent from './Forms/FormTombs'
import falecidoFormComponent from './Forms/FormFalecido'
import FormUpload from './Forms/FormUpload'
export default new Router({

    base: process.env.BASE_URL,
    routes : [
        { path: '/login',name: 'login', component: LoginComponent },
        { path: '/admin',name: 'admin', component: AdminComponent },
        { path: '/register',name: 'register', component: registerComponent },
        { path: '/cemiterio',name: 'cemiterio', component: cemiterioComponent },
        { path: '/tumulo',name: 'tumulo', component: sepulturaComponente },
        { path: '/falecido',name: 'falecido', component: falecidoFormComponent },
        { path: '/lista',name: 'lista', component: listaComponent },
        { path: '/map',name: 'map', component: tombsMapComponent, props: true },
        { path: '/upload',name: 'upload', component: FormUpload, props: true },
        { path: '/',name: 'home', component: homeComponent }
      ]
  })
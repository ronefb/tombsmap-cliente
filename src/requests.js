import axios from "axios";
import Swal from 'sweetalert2';
export default {

    async seachOpenStreet(q){
        var urlOpenStreetMap = axios.OSM_URL + q;
        const res = await axios.get(urlOpenStreetMap);
        return res.data;
    },
    uploadFoto(img, cemierio_id, sepultura_id){
  
      var formData = new FormData();

      formData.append('image', img);
      axios({
        method: 'post',
        url: "cemiterio/sepultura/foto/" + `${cemierio_id}/${sepultura_id}`,
        data: formData,
        headers: { 'Content-Type': 'multipart/form-data' }
      })
      .then(function (res) {
        Swal.fire({
          title: "Sucesso",
          text: res.data,
          icon: "success",
          confirmButtonText: 'OK'
       });
      })
      .catch(function (res) {
         Swal.fire({
          title: "Atenção!",
          text: res.data,
          icon: "error",
          confirmButtonText: 'OK'
       });
      });
    },


}
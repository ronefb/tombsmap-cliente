import Vue from 'vue'

import '../styles/quasar.sass'
import lang from 'quasar/lang/pt-br.js'
import '@quasar/extras/roboto-font/roboto-font.css'
import '@quasar/extras/material-icons/material-icons.css'
import '@quasar/extras/fontawesome-v5/fontawesome-v5.css'
import { Quasar } from 'quasar'

Vue.use(Quasar, {
  config: {
    brand: {
      primary: '#0f13fc',
      secondary: '#75a626',
      accent: '#ffee00',

      dark: '#1d1d1d',

      positive: '#fffb82',
      negative: '#c26700',
      info: '#54ed32',
      warning: '#cf2200'
    }
  },
  extras: [
    'material-icons'
  ],
  plugins:  ['Dialog']
  ,
  lang: lang
 })
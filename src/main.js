import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import store from './store'
import Swal from 'sweetalert2';
import VueMask from 'v-mask';
Vue.use(VueMask);

import 'bootstrap/dist/css/bootstrap.min.css'
import './assets/js/registerServiceWorker'
import './assets/js/quasar'

Vue.config.productionTip = false;

var server = window.location.origin;
if(server.indexOf("localhost") != -1) axios.defaults.baseURL = 'http://localhost/api';
else {
  Vue.config.productionTip = true;
  axios.defaults.baseURL = 'https://www.tombsmap.com.br/api';
}
axios.OSM_URL = 'https://nominatim.openstreetmap.org/search.php?polygon_geojson=1&format=jsonv2&q='

axios.interceptors.request.use(config => {
  if(store.state.token){
    config.headers.Authorization = 'Bearer ' + store.state.token;
  }
  return config;
})

// axios.interceptors.response.use(res => {
//   return res;
// }, error => {
//   if(error.response.status === 403){
//     console.log(error)
//   }else if(error.response.status === 401){
//     store.commit('logout')
//   }
//   throw error;
// })



import { mapState } from 'vuex'
import { mapMutations } from 'vuex'
new Vue({
  router,
  store,
  data(){
    return{
     
    }
  },
  methods: {
    ...mapMutations([
      'logout'
    ]),
    DateBR (value) {
      if (!value) return value;
      
      let t = value.substring(0, 10);
      let h = (value.length > 10) ? ' '+value.substring(11, 16) : '';
      return t.split('-').reverse().join('/') + h;
    },
    alert(message, status){
      var title = status == 'error' ? "Atenção!" :"Sucesso!";
      Swal.fire({
         title: title,
         text: message,
         icon: status,
         confirmButtonText: 'OK'
      });
    }
  },
  computed: {
    ...mapState([
      'usuario',
      'cemiterios',
      'id_cemiterio'
    ])
  },
  render: h => h(App),
}).$mount('#app')



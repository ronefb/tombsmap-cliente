import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import router from './router'
Vue.use(Vuex)

const vuexPersist = new VuexPersist({
  key: 'tombsMapApp',
  storage: localStorage
})

export default new Vuex.Store({
  plugins: [
    vuexPersist.plugin
  ],
  state: {
    usuario: null,
    token: null,
    id_cemiterio: null,
    cemiterio_view: null,
    falecido_view: []
  },
  mutations: {
    setUsuario (state, usuario) {
        state.usuario = usuario
        state.permissao = usuario.permissao[0].nome;
    },
    setToken (state, token)  {
        state.token = token
    },
    setCemiterios(state, cemiterios){
        state.cemiterios = cemiterios
    },
    setCemiterioAdmin(state, id_cemiterio){
      state.id_cemiterio = id_cemiterio
    },
    setCemiterioView(state, cemiterio){
      state.cemiterio_view = cemiterio;
    },
    setFalecido(state, falecido){
      state.falecido_view = falecido;
    },
    logout (state) {
        state.token = null
        state.usuario = null
        state.id_cemiterio = null
        state.cemiterios = null
        state.cemiterio_view = null
        state.permissao = null
        router.push('/login')
    }
  },
  actions: {

  }
})